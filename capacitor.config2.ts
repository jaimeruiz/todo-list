import { CapacitorConfig } from '@capacitor/cli';
const config: CapacitorConfig = {
  appId: 'com.example.app',
  appName: 'ionic-app-base',
  webDir: 'dist',
  server: {
    androidScheme: 'https'
  },
  android:{
    path: '/home/allrivenjs/.local/share/JetBrains/Toolbox/apps/android-studio/bin/studio.sh',
  },

};

export default config;
