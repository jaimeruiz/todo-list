export interface Task {
  name: string;
  subject: string;
  date: string;
  id: number;
  check: boolean;
}

const tasks: Task[] = [
  {
    name: 'Matt Chorsey',
    subject: 'New event: Trip to Vegas',
    date: '9:32 AM',
    id: 0,
    check: true
  },
  {
    name: 'Lauren Ruthford',
    subject: 'Long time no chat',
    date: '6:12 AM',
    id: 1,
    check: false
  },
  {
    name: 'Jordan Firth',
    subject: 'Report Results',
    date: '4:55 AM',
    id: 2,
    check: false
  },
  {
    name: 'Bill Thomas',
    subject: 'The situation',
    date: 'Yesterday',
    id: 3,
    check: false
  },
  {
    name: 'Joanne Pollan',
    subject: 'Updated invitation: Swim lessons',
    date: 'Yesterday',
    id: 4,
    check: false
  },
  {
    name: 'Andrea Cornerston',
    subject: 'Last minute ask',
    date: 'Yesterday',
    id: 5,
    check: false
  },
  {
    name: 'Moe Chamont',
    subject: 'Family Calendar - Version 1',
    date: 'Last Week',
    id: 6,
    check: false
  },
  {
    name: 'Kelly Richardson',
    subject: 'Placeholder Headhots',
    date: 'Last Week',
    id: 7,
    check: false
  }
];

export const getTasks = () => tasks;

export const getTask = (id: number) => tasks.find(t => t.id === id);